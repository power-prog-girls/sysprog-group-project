#!/bin/bash

# Retrieve Keyboard Input
function read_input() {
	local c
	read -p "Choose 1-7: " c
	case $c in
		1)	mute_speaker ;;
		2)	unmute_speaker ;;
		3) 	increase_volume ;;
		4) 	decrease_volume ;;
		5)	record_audio ;;
		6)	play_audio ;;
		7)	echo "See you later!"; exit 0 ;;
		*)	echo "Please select a choice between 1 to 7"
			pause
	esac
}

# Pause Prompt
function pause() {
	local message="$@"
	[ -z $message ] && message="Press [Enter] key to continue..."
	read -p "$message" readEnterKey
}

# Create Header
function create_header() {
	local h="$@"
	echo "====================================="
	echo "${h}"
	echo "====================================="
}

# Display Menu
function show_menu() {
    date
    create_header " AUDIO CONTROLLER & RECORDER "
	echo "1. Mute"
	echo "2. Unmute"
	echo "3. Increase volume"
	echo "4. Decrease volume"
	echo "5. Record an audio"
	echo "6. Play audio files"
    echo "7. Exit"
}

# Option 1: Mute Speaker
function mute_speaker() {
	create_header " Muting Speaker "
	amixer set "Master" "mute"
	echo "Speaker muted!"
	pause
}

# Option 2: Unmute Speaker
function unmute_speaker() {
    create_header " Unmuting Speaker "
	amixer set "Master" "unmute"
	echo "Speaker unmuted!"
	pause
}

# Option 3: Increase Volume
function increase_volume() {
    create_header " Unmuting Speaker "
	amixer -q sset Master 10%+
	echo "Volume increased"
	echo "Volume is now: $(amixer get Master | awk '$0~/%/{print $4}' | tr -d '[]')" 
	pause
}

# Option 4: Increase Volume
function decrease_volume() {
    create_header " Unmuting Speaker "
	amixer -q sset Master 10%-
	echo "Volume decreased"
	echo "Volume is now: $(amixer get Master | awk '$0~/%/{print $4}' | tr -d '[]')" 
	pause
}

# Pause Prompt
function pause() {
 local message="$@"
 [ -z $message ] && message="Press [Enter] key to continue..."
 read -p "$message" readEnterKey
}

# Option 5: Record audio
function record_audio() {
    echo "====================================="
    echo "Record to Mp3"
    echo "====================================="
    read -p 'What is your file name (without file extension)? ' filename
    read -p 'How long do you want to record? ' filesec
    echo "====================================="
    echo "Recording starts!"
    echo "====================================="
    arecord -d $filesec -f U8 $filename.mp3
    echo "======================================="
    echo "Recording finished! Here is your sound:"
    echo "======================================="
    aplay $filename.mp3

    pause
}

# Option 6: Record audio
function play_audio() {
    echo "====================================="
    echo "Play audio"
    echo "====================================="
    read -p 'What is your file name (without file extension)? ' filename
    aplay $filename.mp3
    pause
}

trap '' SIGINT SIGQUIT SIGTSTP

while true
do
	clear
 	show_menu	
 	read_input  
done

# Some codes are taken from: 
# https://bash.cyberciti.biz/guide/Getting_information_about_your_system