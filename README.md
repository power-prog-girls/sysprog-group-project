<div align="center">
<h1>
    Linux Speaker and Mic Driver
</h1>

<h2>
    System Programming, International Class - Odd Semester 2020/2021
    <br>
    Fakultas Ilmu Komputer, Universitas Indonesia
</h2>
</div>

Linux Speaker and Mic Driver is a System Programming project which aims to connect the computer with speaker and mic hardware. The program can mute, unmute, increase volume and decrease volume of the speaker. The program can also record an audio using mic hardware and play it after.

### Prerequisites

Before starting, make sure to have this repository cloned

- git clone https://gitlab.com/power-prog-girls/sysprog-group-project.git

### Installing

For this part, all we need is to run this command
1. chmod +x install.sh
2. sudo sh install.sh

## Team Members

| Role | Name |
|---|---|
| Developer | [Nabila Edina (1806173531)](https://gitlab.com/nabilaedinaa) |
| Developer | [Tsamara Esperanti Erwin (1806173563)](https://gitlab.com/tsamaraes) |
| Developer | [Azzahra Abraara (1806173613)](https://gitlab.com/zahraabr) |

## License

Copyright &copy; 2020 Power Prog Girls
